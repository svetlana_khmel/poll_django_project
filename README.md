 ——————————DJANGO ————————
“command not found: django-admin”¶
django-admin should be on your system path if you installed Django via pip. If it’s not on your path, you can find it in site-packages/django/bin, where site-packages is a directory within your Python installation. Consider symlinking to django-admin from some place on your path, such as /usr/local/bin.
If django-admin doesn’t work but django-admin.py does, you’re probably using a version of Django that doesn’t match the version of this documentation. django-admin is new in Django 1.7.



Install Django on Mac with PIP and VirtualEnv

https://www.youtube.com/watch?v=FshRArXrEcM
Codingforenterpreneurs.com


pip --version
python -m pip install django --user
mkdir django_poll
cd django_poll/
django-admin startproject django_poll
python
virtualenv
sudo pip install virtualenv
virtualenv testenv
cd testenv/
ls
source bin/activate
pip freeze
pip install Django==3.0.3
pip freeze // see dependencies
pip install django --upgrade
django-admin.py startproject poll_django_project
ls
cd poll_django_project
python manage.py runserver


Tutorial
https://docs.djangoproject.com/en/3.0/intro/tutorial01/


By default, the configuration uses SQLite. If you’re new to databases, or you’re just interested in trying Django, this is the easiest choice. SQLite is included in Python, so you won’t need to install anything else to support your database.

1261


There are a few steps to see the tables in an SQLite database:
1. List the tables in your database: .tables 
2. List how the table looks: .schema tablename 
3. Print the entire table: SELECT * FROM tablename; 
4. List all of the available SQLite prompt commands: .help 
>> .database
>> .quit
Tutorial SQLite https://www.youtube.com/watch?v=iyXYwNQC6ag


Superuser: admin
admin123admin
 
 
 
  552  python manage.py runserver
  553  python manage.py migrate
  554  python manage.py runserver
  555  python manage.py makemigrations polls
  556  python manage.py sqlmigrate polls 0001
  557   python manage.py chec
  558   python manage.py checл
  559   python manage.py check
  560  python manage.py migrate
  561  python manage.py shell
  562  python manage.py shell
  563  python manage.py createsuperuser
  564  python manage.py runserver
  565  python manage.py runserver
  566  python manage.py runserver
  567  python manage.py test polls
  568  python manage.py test polls
  569  python manage.py test polls
  570  python manage.py test polls
  571  python manage.py test polls
  
  
  How to write tests django
  https://docs.djangoproject.com/en/3.0/intro/tutorial05/
  
  
  Django with databases:
  https://docs.djangoproject.com/en/3.0/intro/tutorial02/
  https://docs.djangoproject.com/en/3.0/ref/databases/#sqlite-notes
  https://docs.djangoproject.com/en/3.0/topics/db/
  
  Urls:
  https://docs.djangoproject.com/en/3.0/topics/http/urls/
  
  